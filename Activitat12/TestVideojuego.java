package Activitat12;

public class TestVideojuego {
    public static void main(String[] args) {
        Videojuego[] videojuegos =  new Videojuego[5];
        videojuegos[0] = new Videojuego("Fortnite", "Acción", 40, true);
        videojuegos[1] = new Videojuego("Fifa", "Deportes",50,true);
        videojuegos[2]= new Videojuego("Gran Theft Auto","Acción",80,true);
        videojuegos[3]= new Videojuego("Minecraft","Simulación",60,true);
        videojuegos[4]= new Videojuego("AnimalCrossing","Simulacion",30,true);


        Videojuego videojuegoMasBarato = videojuegoMasBarato(videojuegos);
        System.out.println("El videojuego más barato es: " + videojuegoMasBarato.getTitulo()+" con un precio de "+videojuegoMasBarato.getPrecio()+"€");
        Videojuego videojuegoMasCaro = videojuegoMasCaro(videojuegos);
        System.out.println("El videojuego más caro es: " + videojuegoMasCaro.getTitulo()+" con un precio de "+videojuegoMasCaro.getPrecio()+"€");
        System.out.println("El coste total de los videojuegos es "+costeTotalVideojuegos(videojuegos)+"€");
    }

    public static Videojuego videojuegoMasBarato(Videojuego[] videojuegos) {

        Videojuego masBarato = null;
        int precioMasBarato = 10000;
        for (int i = 0; i < videojuegos.length; i++) {

            if (videojuegos[i].getPrecio() < precioMasBarato) {
                masBarato = videojuegos[i];
                precioMasBarato = videojuegos[i].getPrecio();
            }
        }

        return masBarato;
    }

    public static Videojuego videojuegoMasCaro(Videojuego[] videojuegos) {

        Videojuego masCaro = null;
        int precioMasCaro = 10;
        for (int i = 0; i < videojuegos.length; i++) {

            if (videojuegos[i].getPrecio() > precioMasCaro) {
                masCaro = videojuegos[i];
                precioMasCaro = videojuegos[i].getPrecio();
            }
        }

        return masCaro;
    }

    public static int costeTotalVideojuegos(Videojuego[] videojuegos) {
        int total=0;
        for (int i = 0; i < videojuegos.length; i++) {
            total=total+videojuegos[i].getPrecio();
        }
        return total;
    }
}
