package Activitat12;

public class Videojuego {
    private String titulo;
    private String genero;
    private int precio;
    private boolean multijugador;


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isMultijugador() {
        return multijugador;
    }

    public void setMultijugador(boolean multijugador) {
        this.multijugador = multijugador;
    }

    public Videojuego(String titulo, String genero, int precio, boolean multijugador) {
        this.titulo = titulo;
        this.genero = genero;
        this.precio = precio;
        this.multijugador = multijugador;


    }
}

