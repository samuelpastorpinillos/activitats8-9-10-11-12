package Activitat11;

public class NIF {
    private String dni;
    private String letra;



    public NIF(String dni) {
        this.dni = dni;
        this.letra = calcularLetra();
    }
    public String getNif(){
        return this.dni+this.letra;
    }

    public String getDni() {
        return this.dni;
    }

    public String getLetra() {
        return this.letra;
    }

    private String calcularLetra() {
        char [] letras = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
        int numDni = Integer.valueOf(this.dni);
        int resto = numDni % 23;
        return String.valueOf(letras[resto]);
    }

}
