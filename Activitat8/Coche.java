package Activitat8;

public class Coche {

    private String marca;
    private String modelo;
    private String color;
    private boolean pintura_metalizada;
    private String matricula;
    private int año_fabricación;

    public Coche(String marca, String modelo, String color, boolean pintura_metalizada, String matricula, int año_fabricación) {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.pintura_metalizada = pintura_metalizada;
        this.matricula = matricula;
        this.año_fabricación = año_fabricación;
    }
    public Coche() {
        this.marca = "Ford";
        this.modelo = "Supra";
        this.color = "Amarillo";
        this.pintura_metalizada = true;
        this.matricula = "PCR2016";
        this.año_fabricación = 1945;
    }

    public void mostrarInfo() {
        System.out.println("\n\nMarca: " + this.marca);
        System.out.println("Modelo: " + this.modelo);
        System.out.println("Color: " + this.color);
        System.out.println("Pintura metalizada: " + this.metalizada());
        System.out.println("Matricula: " + this.matricula);
        System.out.println("Año de fabricación: " + this.año_fabricación);
    }
    private String metalizada() {
        if (this.pintura_metalizada) {
            return "Si";
        }

        return "No";
    }
}
