package Activitat8;

public class TestCoche {
    public static void main(String[] args) {
        Coche ch1 = new Coche("Seat","Ibiza",
                "Rojo",true,"2212KVN",
                2000);
        Coche ch2 = new Coche("Ferrari","Modena",
                "Negro",true,"2122RRN",
                1985);
        Coche ch3 = new Coche("Volksvagen","Tiguan",
                "Blanco",false,"1212KJ",
                2015);
        Coche ch4 = new Coche("Opel","Corsa",
                "Verde",true,"56355R",
                2003);

        Coche ch5 = new Coche();

        ch1.mostrarInfo();
        ch2.mostrarInfo();
        ch3.mostrarInfo();
        ch4.mostrarInfo();
        ch5.mostrarInfo();
    }
}

