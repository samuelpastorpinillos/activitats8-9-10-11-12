package Activitat10;

public class CuentaBancaria {
    private String nombre;
    private String numero_cuenta;
    private double interes;
    private double saldo;
    private boolean estado;

    public CuentaBancaria(String nombre, String numero_cuenta, double interes, double saldo, boolean estado) {
        this.nombre = nombre;
        this.numero_cuenta = numero_cuenta;
        this.interes = interes;
        this.saldo = saldo;
        this.estado = estado;
    }

  public CuentaBancaria(String nombre, String numero_cuenta) {
      this.nombre = nombre;
      this.numero_cuenta = numero_cuenta;
      this.interes = 0.1;
      this.saldo = 0;
      this.estado = true;

  }
  boolean reintegro(float cantidad) {
        if (estado == true) {
            saldo = saldo - cantidad;
            return true;

        }
        return false;
  }
  boolean ingreso (float cantidad) {
      if (estado == true) {
          saldo = saldo + cantidad;
          return true;

      }
      return false;

  }

    public String getNombre() {
        return nombre;
    }

    public String getNumero_cuenta() {
        return numero_cuenta;
    }

    public double getInteres() {
        return interes;
    }

    public double getSaldo() {
        return saldo;
    }

    public boolean isEstado() {
        return estado;
    }
    public void setEstado(boolean estado1) {

        this.estado = estado1;
    }
}

