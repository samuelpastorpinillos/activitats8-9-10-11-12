package Activitat10;

public class TestCuenta {
    public static void main(String[] args) {
        CuentaBancaria as1 = new CuentaBancaria("Pepe perez","00010021");
        as1.ingreso(1000);
        as1.reintegro(100);
        System.out.println("----- TEST Cuenta -----");
        System.out.println("Cuenta: "+as1.getNumero_cuenta()+", Cliente: "+as1.getNombre()+", interés: "+ as1.getInteres()+"%, Saldo: "+ as1.getSaldo()+"€");

        CuentaBancaria as2 = new CuentaBancaria("Juan Sanchez","214521421",0.5,30000,true);
        as2.setEstado(false);
        as2.reintegro(500);
        System.out.println("Cuenta: "+as2.getNumero_cuenta()+", Cliente: "+as2.getNombre()+", interés: "+ as2.getInteres()+"%, Saldo: "+ as2.getSaldo()+"€");

    }

}
