package Activitat9;

public class TestAsignatura {
    public static void main(String[] args) {
        Asignatura as1 = new Asignatura("Matematicas",1017,1,true);
        System.out.println("Nombre: " + as1.getNombre());
        System.out.println("Codigo: " + as1.getCodigo());
        System.out.println("Curso: " + as1.getCurso());
        System.out.println("Optativa: " + as1.isOptativo());
        as1.setOptativo(false);
        System.out.println("\nCambiado optativa");
        System.out.println("Nombre: " + as1.getNombre());
        System.out.println("Codigo: " + as1.getCodigo());
        System.out.println("Curso: " + as1.getCurso());
        System.out.println("Optativa: " + as1.isOptativo());




    }
}
