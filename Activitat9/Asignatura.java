package Activitat9;

public class Asignatura {

    private String nombre;
    private int codigo;
    private int curso;
    private boolean optativo;

    public Asignatura(String nombre, int codigo, int curso, boolean optativo) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.curso = curso;
        this.optativo = optativo;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getCurso() {
        return curso;
    }

    public boolean isOptativo() {
        return optativo;
    }

    public void setOptativo(boolean esOptativo) {
        this.optativo = esOptativo;
    }
}
